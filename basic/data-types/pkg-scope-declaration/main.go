package main

import "fmt"

// Cannot use short declaraction at package scope,
// but you can use type inference at package scope
// danger := true
var danger = true

func main() {
	// Package scope and declaration
	// - At the package scope: You can only use declaractions
	//   that start with keywords (e.g., package, import, func,...)
	fmt.Println(danger)

	// Multiple short declarations
	// - Assignments are based off declared position
	license, money := true, 5000
	fmt.Printf("license: %v, money: %v\n", license, money)

	// Redeclarations
	name := "Nikola"
	fmt.Println(&name)
	name, age := "Marie", 60
	fmt.Println(name, age)
	fmt.Println(&name) // Uses same memory address

	name = "Albert" // Assignment
	birth := 1897   // Declares a new variable
	fmt.Println(name, birth)

	// When to use a short declaration?
	// Use `var identifier type` (e.g., var status string)
	// - If you don't know the initial value
	// - When you need a package scoped variable
	// - When you want to group variables together for greater readability
	var (
		// related
		video string

		// closely related
		duration int
		current  int
	)
	fmt.Println(video, duration, current)

	// Use `identifier := value` (e.g., status := "pass")
	// - If you know the initial value
	// - To keep the code consise
	// - For redeclaration
}
