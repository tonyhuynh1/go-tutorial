package main

import "fmt"

func main() {
	// Type Conversion
	// - [SYNTAX] type(value)
	//   - type: Name of the type. Changes the type of a given value into this type
	//   - value: value to be converted

	speed := 100 // `speed` is an int
	force := 2.5 // `force` is a float64

	// [ERROR] invalid operation: speed * force (mismatched types int and float64)
	// speed = speed * force

	// If we convert force to an int then 2.5 => 2
	// - [!] This conversion can be a destructive operation
	// - force variable is still 2.5
	// speed = speed * int(force)
	// fmt.Println(speed)

	// Recommended
	// - Convert speed from `int` to `float64`
	// - Convert resultant of speed * force to `int`
	// - [!] This conversion will truncate the values (e.g., force := 2.513, then result = 251, which is missing the 3)
	speed = int(float64(speed) * force)
	fmt.Println(speed)

	// Types with different names are different types
	// - (e.g., int vs int32 are different)
	var apple int
	var orange int32

	_ = apple
	// [ERROR] cannot use orange (type int32) as type int in assignment
	// apple = orange
	// [ERROR] cannot use apple (type int) as type int32 in assignment
	// orange = apple

	apple = int(orange)

	// Converting int to string
	// - [!] cannot convert other numeric types to string

	// [ERROR] cannot convert 65 (type untyped number) to type string
	// fmt.Println(string(65.0))
	fmt.Println(string(65))               // Converts ascii 65 -> 'A'
	fmt.Println(string([]byte{104, 105})) // 104 -> 'h', 105 -> 'i'
}
