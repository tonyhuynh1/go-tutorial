package main

import "fmt"

func main() {
	// Variable initialization
	// - Initialization
	var safe bool = true
	fmt.Printf("safe (%T): %v\n", safe, safe)
	// - Type inference
	var fire = true
	fmt.Println(fire)
	// - Short declaraction
	lit := true
	fmt.Printf("This mixtape is lit: %v\n", lit)
}
