package main

import "fmt"

import "time"

func main() {
	// Assignment
	var acceleration int
	fmt.Println(acceleration) // => 0 (numeric zero value)
	acceleration = 100
	fmt.Println(acceleration) // => 100
	acceleration -= 25        // Same as acceleration = acceleration - 25
	fmt.Println(acceleration) // => 100 - 25 = 75

	// - Strongly Typed
	// - You can't assign a value with a type to a variable with another type
	var position int
	// position = "100"		// "100" is string but `position` is int
	var running bool
	// running = 1			// 1 is int but `running` is bool
	_, _ = position, running
	var force float64
	// position = force		// `position` is int but `force` is float64
	force = 1
	fmt.Println(force) // => 1

	// Multiple assignments
	var (
		speed int
		now   time.Time
	)

	speed, now = 100, time.Now()
	fmt.Println(speed, now)

	// - Swapping
	var (
		currentSpeed  = 100
		previousSpeed = 50
	)

	currentSpeed, previousSpeed = previousSpeed, currentSpeed
	fmt.Println(currentSpeed, previousSpeed) // => 50, 100
}
