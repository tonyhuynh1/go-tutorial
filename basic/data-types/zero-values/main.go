package main

import "fmt"

func main() {
	// Zero values
	// an empty variable gets a zero value depending on its type
	// ----------------------
	// booleans -> false
	// numerics -> 0
	// strings  -> ""
	// pointers -> nil
	// ----------------------

	// Mutliple declarations
	var (
		velocity int
		heat     float64
		status   bool
		brand    string
	)
	// OR
	var test4, test5 int
	fmt.Println(test4, test5)

	fmt.Println(velocity)     // 0
	fmt.Println(heat)         // 0
	fmt.Println(status)       // false
	fmt.Printf("%q\n", brand) // ""
}
