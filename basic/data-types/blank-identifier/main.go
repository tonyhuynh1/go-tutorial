package main

func main() {
	// Blank identifier
	// - The character "_" ingests all values assigned to it
	var test3 int
	_ = test3
}
