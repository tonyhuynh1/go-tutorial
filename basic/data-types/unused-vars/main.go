package main

import "fmt"

var test1 int // Declared at main package's scope

func main() {
	// Unused variables
	var test2 int
	fmt.Println(test2)
	// - Variables declared in a block scope needs to be used (e.g., test2)
	// - Variables declared in a package scope does not need to be used (e.g., test1)
}
