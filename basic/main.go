package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("Hello Gopher!")
	bye() // Same package "main" scope
	hey() // Same package "main" scope

	fmt.Println(runtime.NumCPU())
}
