package printer

import (
	"fmt"
	"runtime"
)

// $ go install
// => /Users/tonyhuynh/go/pkg/darwin_amd64/gitlab.com/tonyduy092/go-tutorial/basic/printer.a

// Hello is an exported function
func Hello() {
	fmt.Println("Hello")
}

// Version is an exported function
func Version() string {
	return runtime.Version()
}
