package main

import (
	"fmt"

	"gitlab.com/tonyduy092/go-tutorial/basic/printer"
)

func main() {
	printer.Hello()
	fmt.Println(printer.Version())
}
