package main

import (
	"fmt"
	"path"
)

func main() {
	var dir1, file1 string
	dir1, file1 = path.Split("res/css/main.css")
	fmt.Println(dir1, file1)

	dir2, file2 := path.Split("basic/data-types/main.go")
	fmt.Println(dir2, file2)

	// Disgarded dir returned from path.Split using "_"
	_, file3 := path.Split("src/java/MainActivity.java")
	fmt.Println("file: ", file3)
}
